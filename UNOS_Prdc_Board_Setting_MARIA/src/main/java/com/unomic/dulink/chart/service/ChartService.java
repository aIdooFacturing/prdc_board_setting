package com.unomic.dulink.chart.service;

import com.unomic.dulink.chart.domain.ChartVo;


public interface ChartService {
	public String getTgCnt(ChartVo chartVo) throws Exception;
	public String addTgCnt(String val, ChartVo chartVo) throws Exception;
	
	//common func
	public String getAppList(ChartVo chartVo) throws Exception;
	public String removeApp(ChartVo chartVo) throws Exception;
	public String addNewApp(ChartVo chartVo) throws Exception;
	public String login(ChartVo chartVo) throws Exception;
	public String getStartTime(ChartVo chartVo) throws Exception;
	public String getComName(ChartVo chartVo) throws Exception;
	public ChartVo getBanner(ChartVo chartVo) throws Exception;
	public String getCirTime() throws Exception;
	public String setCirTime(ChartVo chartVo) throws Exception;
	public String getDvc(ChartVo chartVo) throws Exception;
	public String setSelDvc(String val) throws Exception;
	public String getSelDvc() throws Exception;
	
};
