
package com.unomic.dulink.chart.domain;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;




@Getter
@Setter
@ToString
public class ChartVo{
	String tgCnt;
	String cntPerCyl;
	String tgRunTime;
	String workIdx;
	String workDate;
	String cirTime;
	
	//common
	String ty;
	String fileLocation;
	String url;
	String fileName;
	String appId;
	String appName;
	String categoryId;
	Integer shopId;
	String dvcId;
	String id;
	String pwd;
	String name;
	String msg;
	String rgb;
}
